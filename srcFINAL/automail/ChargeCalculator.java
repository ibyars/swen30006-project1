package automail;

import java.util.HashMap;
import java.util.Map;

public class ChargeCalculator {
	/*Stores the last service fee for each floor*/
	private static Map<Integer, Double> lastServiceFee = new HashMap<>();
	private double markup;
	private double unitPrice;
	private int mailroomLocation;


	
    /*Number of activity units associated with each activity*/
    static private final double MOVEMENT_ACTIVITY_UNITS = 5;
    static private final double LOOKUP_ACTIVITY_UNITS = 0.1;

	
	public ChargeCalculator(int numFloors, double markup, double unitPrice, int mailroomLocation) {
		this.markup = markup;
		this.unitPrice = unitPrice;
		this.mailroomLocation = mailroomLocation;
		
		// initialise service fees to 0 for all floors
		for(int i = 1; i < numFloors + 1; i++) {
			lastServiceFee.put(i, 0.0);
		}
		
	}
	
	/**
	 * Calculates the activity cost for the parameter item
	 * Activity cost = activity units * unit price
	 * May be altered to factor in weight or delays in the future
	 * @param item the mail item whos activity cost is calculated
	 * */
	public double calculateActivityCost(MailItem item) {
		return item.getActivityUnits() * unitPrice;
	}
	
	/**
	 * Returns the expected cost for an item using the last calculated service fee
	 * @param item The item whos expected cost is calculated
	 * */
	public double calculateExpectedCharge(MailItem item) {
		
		double floorsToTravel = Math.abs(item.getDestFloor() - mailroomLocation) * 2; 
		double expectedActivityUnits = floorsToTravel*MOVEMENT_ACTIVITY_UNITS + LOOKUP_ACTIVITY_UNITS;
		double expectedCost = expectedActivityUnits * unitPrice + lastServiceFee.get(item.getDestFloor());
		
		return expectedCost * (1 + markup);
	}
	
	/**
	 * Returns the cost for an item
	 * @param item The item whos cost is calculated
	 * */
	public double calculateCost(MailItem item) {		
		return calculateActivityCost(item) + item.getServiceFee();
	}
	
	/**
	 * 
	 * @return delivery charge for the item
	 * */
	public double getCharge(MailItem item) {
		return calculateCost(item) * (1 + markup);
	}
	
	/**
	 * Update the last calculated service fee
	 * @param floor the floor for which a new service fee has been looked up
	 * @param fee the new service fee
	 * */
	public void updateServiceFee(int floor, double fee) {
		if (fee > 0) {
			lastServiceFee.replace(floor, fee);
		}
	}
	
	/**
	 * 
	 * @param floor the floor to retrieve the fee for
	 * @return the last service fee charged at the given floor
	 */
	public double getFeeForFloor(int floor) {
		return lastServiceFee.get(floor);
	}


	
	/**
	 * 
	 * @return the building's mailroom floor
	 */
	public int getMailroomFloor () {
		return mailroomLocation;
	}

}
