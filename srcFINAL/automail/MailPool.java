package automail;

import java.util.LinkedList;
import java.util.Comparator;
import java.util.ListIterator;

import exceptions.ItemTooHeavyException;

/**
 * addToPool is called when there are mail items newly arrived at the building to add to the MailPool or
 * if a robot returns with some undelivered items - these are added back to the MailPool.
 * The data structure and algorithms used in the MailPool is your choice.
 *
 */
public class MailPool {



	private class Item {
		int destination;
		MailItem mailItem;
		// Use stable sort to keep arrival time relative positions

		public Item(MailItem mailItem) {
			destination = mailItem.getDestFloor();
			this.mailItem = mailItem;
		}
	}

	public class ItemComparator implements Comparator<Item> {
		@Override
		//method to add items with expected charge>charge threshold to new linked list

		public int compare(Item i1,Item i2) {
			
			/*comparison is based on the expected charges of the mail items*/
			double i1Charge = MailItem.chargeCalculator.calculateExpectedCharge(i1.mailItem);
			double i2Charge = MailItem.chargeCalculator.calculateExpectedCharge(i2.mailItem);

			int order = 0;
			// Items above the charge threshold will be delivered first. When both items are above (or both items are below) the 
			// charge threshold, items with the farthest destination floors are delivered first. 
			if ((i1Charge > chargeThreshold) && (i2Charge < chargeThreshold)) {
				order = -1;
			} else if ((i2Charge > chargeThreshold) && (i1Charge < chargeThreshold)) {
				order = 1;
			} else if (i1.destination<i2.destination){
				order = 1;
			} else if (i1.destination>i2.destination){
				order = -1;
			}

			return order;

		}
	}

	private LinkedList<Item> pool;
	private LinkedList<Robot> robots;
	private double chargeThreshold;

	public MailPool(int nrobots, double chargeThreshold){
		// Start empty
		pool = new LinkedList<Item>();
		robots = new LinkedList<Robot>();
		this.chargeThreshold = chargeThreshold;
	}

	/**
	 * Adds an item to the mail pool
	 * @param mailItem the mail item being added.
	 */
	public void addToPool(MailItem mailItem) {
		Item item = new Item(mailItem);
		pool.add(item);
		pool.sort(new ItemComparator());
		
	}

	/**
	 * load up any waiting robots with mailItems, if any.
	 */
	public void loadItemsToRobot() throws ItemTooHeavyException {
		
		//List available robots
		ListIterator<Robot> i = robots.listIterator();
		while (i.hasNext()) loadItem(i);
	}

	//load items to the robot
	private void loadItem(ListIterator<Robot> i) throws ItemTooHeavyException {
		Robot robot = i.next();
		assert(robot.isEmpty());
		// System.out.printf("P: %3d%n", pool.size());
		ListIterator<Item> j = pool.listIterator();
		if (pool.size() > 0) {
			try {
				robot.addToHand(j.next().mailItem); // hand first as we want higher priority delivered first
				j.remove();
				if (pool.size() > 0) {
					robot.addToTube(j.next().mailItem);
					j.remove();
				}
				robot.dispatch(); // send the robot off if it has any items to deliver
				i.remove();       // remove from mailPool queue
			} catch (Exception e) {
				throw e;
			}
		}
	}

	/**
	 * @param robot refers to a robot which has arrived back ready for more mailItems to deliver
	 */
	public void registerWaiting(Robot robot) { // assumes won't be there already
		robots.add(robot);
	}

	/**
	 *
	 * @return the charge threshold associated with this mailpool
	 */

	public double getChargeThreshold () {
		return this.chargeThreshold;
	}

}


