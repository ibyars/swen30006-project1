package automail;

import java.util.Map;
import java.util.TreeMap;

// import java.util.UUID;

/**
 * Represents a mail item
 */
public class MailItem {
	
    /** Represents the destination floor to which the mail is intended to go */
    protected final int destination_floor;
    /** The mail identifier */
    protected final String id;
    /** The time the mail item arrived */
    protected final int arrival_time;
    /** The weight in grams of the mail item */
    protected final int weight;
    
    /* The number of activity units associated with delivering this mail item*/
    private double activity_units;
    
    /* The service fee looked up at time of delivery*/
    private double serviceFee;
    private Boolean lookupSuccessful;
    
    /* The charge threshold associated with this run of the simulation*/ 
    private double chargeThreshold;
    /* Charge calculator helper class to calculate the charge of each item */
    public static ChargeCalculator chargeCalculator;

    /**
     * Constructor for a MailItem
     * @param dest_floor the destination floor intended for this mail item
     * @param arrival_time the time that the mail arrived
     * @param weight the weight of this mail item
     */
    public MailItem(int dest_floor, int arrival_time, int weight, double chargeThreshold){
        this.destination_floor = dest_floor;
        this.id = String.valueOf(hashCode());
        this.arrival_time = arrival_time;
        this.weight = weight;
        
        /*Initialise activity units and costs to zero*/
        this.activity_units = 0;
        this.serviceFee = 0;
        this.lookupSuccessful = false;
        
        this.chargeThreshold = chargeThreshold;
    }
    
    /** 
     * Configure charge calculator class with data from simulation
     * @param numFloors the number of floors in the building
     * @param markup the markup percentage
     * @param unitPrice the price per activity unit
     * */
    public static void configureChargeCalculator(int numFloors, double markup, double unitPrice, int mailroomLocation) {
    	chargeCalculator = new ChargeCalculator(numFloors, markup, unitPrice, mailroomLocation);
    }

    /**
     * Return a string representation of MailItem (does not include charge information)
     * */
    @Override
    public String toString(){
    	return String.format("Mail Item:: ID: %6s | Arrival: %4d | Destination: %2d | Weight: %4d", id, arrival_time, 
    				destination_floor, weight);
    }
    
    /**
     * Modified version of the toString() method that includes information related to the charge if charge threshold and charge display
     * are set
     * 
     */
    public String toStringWithCharge(boolean chargeDisplay){

    	if (chargeDisplay & (chargeThreshold != 0)) {
    		return String.format("Mail Item:: ID: %6s | Arrival: %4d | Destination: %2d | Weight: %4d | Charge: %.2f | Cost: %.2f | Fee: %.2f | Activity: %.2f", 
    				id, arrival_time, destination_floor, weight, chargeCalculator.getCharge(this), chargeCalculator.calculateCost(this), 
    				serviceFee, activity_units);
    	}
    	return String.format("Mail Item:: ID: %6s | Arrival: %4d | Destination: %2d | Weight: %4d", id, arrival_time, 
				destination_floor, weight);
    }


    /**
     *
     * @return the destination floor of the mail item
     */
    public int getDestFloor() {
        return destination_floor;
    }
    
    /**
     *
     * @return the ID of the mail item
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @return the arrival time of the mail item
     */
    public int getArrivalTime(){
        return arrival_time;
    }

    /**
    *
    * @return the weight of the mail item
    */
   public int getWeight(){
       return weight;
   }
   
   /**
    * Increases the activity unit counter
    * @param num_units the number of units to add
    */
   public void addActivityUnits(double num_units) {
	   this.activity_units += num_units;
   }
   
   /**
    * 
    *  @return the number of activity units incurred
    */
   public double getActivityUnits() {
	   return activity_units;
   }
   
   /**
    * 
    *  @return the service fee paid on delivery of this item
    */
   public double getServiceFee() {
	   return serviceFee;
   }
   
   /**
    * 
    * @return boolean of whether the service fee lookup was successful for this item
    */
   public boolean getLookupSuccess() {
	   return lookupSuccessful;
   }
   
   /**
    * 
    * @return the activity cost associated with delivering this item
    */
   public double getActivityCost() {
	   return chargeCalculator.calculateActivityCost(this);
   }

   
   /** 
    * Set the service fee for the mail item. If trying to give a service fee that is less than zero, service fee is set as the
    * last known service fee of that floor.
    * 
    * @param fee retrieved during the lookup
    */
   public void setServiceFee(double fee) {
	   if (fee >= 0) {
		   //set the service fee, and mark the lookup as successful
		   serviceFee = fee;
		   lookupSuccessful = true;
		   
		   //save this fee as the last known fee for the current floor
		   MailItem.chargeCalculator.updateServiceFee(destination_floor, fee);
	   }
	   else {
		   
		   lookupSuccessful = false;
		   
		   //use the last known service fee for the current floor as the service fee (DELETE THIS IF USING FEE = 0)
		   serviceFee = chargeCalculator.getFeeForFloor(destination_floor);
		   //System.out.format("LOOKUP FAILED\nUsing last known fee (%.2f) for floor %d\n", serviceFee, destination_floor);
	   }
   }
   
	static private int count = 0;
	static private Map<Integer, Integer> hashMap = new TreeMap<Integer, Integer>();

	@Override
	public int hashCode() {
		Integer hash0 = super.hashCode();
		Integer hash = hashMap.get(hash0);
		if (hash == null) { hash = count++; hashMap.put(hash0, hash); }
		return hash;
	}

}
