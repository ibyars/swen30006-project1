package simulation;

public class StatisticsTracker {
	private int totalItems;
	private double billableActivity;
	private double totalActivityCost;
	private double totalServiceCost;
	private int[] lookupSuccess;
	
	// constant variables for lookupSuccess to increment successes at index 0 and failures at index 1
	private final int success_index = 0;
	private final int failure_index = 1;

	public StatisticsTracker() {
		totalItems = 0;
		billableActivity = 0;
		totalServiceCost = 0;
		
		lookupSuccess = new int[2];
		lookupSuccess[success_index] = 0;
		lookupSuccess[failure_index] = 0;
	}
	
	
	/**
	 * Update the statistics saved for the mail run (to be called upon delivery of one package)
	 */
	public void updateStatistics(double billableActivity, double activityCost, double serviceCost, boolean lookupSuccessful) {
	
		this.totalItems += 1;
		this.totalActivityCost += activityCost;
		this.billableActivity += billableActivity;
		this.totalServiceCost += serviceCost;
		
		if (lookupSuccessful) {
			lookupSuccess[success_index] += 1;
		} else {
			lookupSuccess[failure_index] += 1;
		}
	}
	
	/**
     * Print statistics - to be called at the end of the mail run if chargeDisplay is toggled on.
     */
    public void printStatistics() {
    	System.out.println("Number of items delivered: " + totalItems);
    	System.out.format("Total billable activity: %.2f\n", billableActivity);
    	System.out.format("Total activity cost: %.2f\n", totalActivityCost);
    	System.out.format("Total service cost: %.2f\n", totalServiceCost);
    	
    	int numLookups = lookupSuccess[success_index] + lookupSuccess[failure_index];
    	System.out.println("Number of lookups: " + numLookups + " [" + lookupSuccess[failure_index] + " failed, " 
    			+ lookupSuccess[success_index] + " successful]" );

    }
}
